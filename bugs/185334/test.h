
typedef int ReturnType;
typedef int ParameterType;


class Test {
public:
    Test();
    ReturnType method(ParameterType test);
};

