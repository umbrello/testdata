
class QGraphicsObject;

class TestClass;
class TestClass2;

class WidgetBase : public QGraphicsObject {
public:
    WidgetBase();

private:
    TestClass m_testMember;
    TestClass2 *m_anotherTestMember;
  
};
  
