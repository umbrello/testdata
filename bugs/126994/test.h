#ifndef POSEDON_FS_H
#define POSEDON_FS_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/mman.h>

#include <vector>
#include <string>
#include <algorithm>

#include <iostream>


using namespace std;

class Fs;

/*!
 * @class File
 * @brief �������� ���������� � �����
 */
class File
{
protected:
	string  name;							//!< ��� �����
public:
	/*!
	 * @fn File(string the_name)
	 * @brief ����������� ����'�
	 * @param the_name ���
	 */
	File(string the_name);

	/*! 
	 * @fn string get_name()
	 * @brief ��������� ��� ����
	 * @return ���
	 */
	string  get_name();
	 
	/*!
	 * @fn void    set_name(string the_name)
	 * @brief ������� ��� ����
	 * @param the_name ����� ���
	 */
	void    set_name(string the_name);
};


/*!
 * @class Block
 * @brief �������� ���������� � ������ ������
 */
class Block
{
	int start;				//!< ������ �����
	int length;				//!< ����� �����
	int n_next_block;		//!< ����� ���������� �����
	int use;				//!< ������� ������������ �� ���� ����
public:
	/*!
	 * @fn Block()
	 * @brief ����������� �� ���������
	 */
	Block();
	
	/*!
	 * @fn Block(int the_start, int the_length, int the_n_next_block, int the_use)
	 * @brief ����������� ����� � ��������� �������
	 * @param the_start ������
	 * @param the_length �����
	 * @param the_n_next_block ����� ���������� �����
	 * @param the_use �������
	 */
	Block(int the_start, int the_length, int the_n_next_block, int the_use);
	
	/*!
	 * @fn void saving(FILE *f)
	 * @brief ��������� ������ ����� �� ����
	 * @param f �������� ��� ������ ���������� �����
	 */
	void saving(FILE *f);
	
	/*!
	 * @fn void loading(FILE *f)
	 * @brief �������� ������ � ����� �� �����
	 * @param f �������� ��� ������ ���������� �����
	 */
	void loading(FILE *f);
	 
	friend class Fs;
};

/*!
 * @class Directory
 * @brief �������� ���������� � ��������
 */
class Directory : public File
{
	Fs *fs;					//!< ��������� �� ��
	int this_dir;			//!< ����� ������� ��������

	vector<int> Files;		//!< ������ ������� ������ ����������� � ������ ��������
	vector<int> Dirs;		//!< ������ ������� ����������� ����������� � ������ ��������

public:
	/*!
	 * @fn Directory(Fs *the_fs, string the_name, int the_this_dir)
	 * @brief ���������� ����������
	 * @param the_fs ��������� �� ��
	 * @param the_name ��� ����������
	 * @param the_this_dir ����� ��������
	 */
	Directory(Fs *the_fs, string the_name, int the_this_dir);
	
	/*!
	 * @fn int has_dir(string the_dir)
	 * @brief �������� ���������� �� � �������� �������� ���������� � ������ ������
	 * @param the_dir ��� ����������
	 * @return ����� ��� -1
	 */
	int has_dir(string the_dir);
	
	/*!
	 * @fn int has_file(string the_file)
	 * @brief �������� ���������� �� � �������� �������� ����� � ������ ������
	 * @param the_file ��� �����
	 * @return ����� ��� -1
	 */
	int has_file(string the_file);

	/*!
	 * @fn void connect_dir(int the_dir)
	 * @brief "c��������" ������� � ���������
	 * @param the_dir ����� ��������
	 */
	void connect_dir(int the_dir);
	
	/*!
	 * @fn void connect_file(int the_file)
	 * @brief "��������� ������� � ������"
	 * @param the_file ����� ����
	 */
	void connect_file(int the_file);
	
	/*! 
	 * @fn vector<string> print()
	 * @brief ��������� ���������� ������� ��������
	 * @return vector ����� ����������
	 */
	vector<string>	print();
	
	/*!
	 * @fn vector<string> print_tree(string prefix)
	 * @brief ��������� ����������� ���������� ������� ��������
	 * @param prefix ����� �������� ������� ������� �������� ����� �������
	 * @return vector ����� ����������
	 */
	vector<string> print_tree(string prefix);

	/*!
	 * @fn int get_n_dir(string name_dir)
	 * @brief ��������� ����� ���������� �� �����
	 * @param name_dir ��� ����������
	 * @return ����� ��� -1
	 */
	int get_n_dir(string name_dir);
	
	/*!
	 * @fn int get_n_file(string name_file)
	 * @brief ��������� ����� ����� �� �����
	 * @param name_file ��� �����
	 * @return ����� ��� -1
	 */
	int get_n_file(string name_file);
		
	/*!
	 * @fn void saving(FILE *f)
	 * @brief ��������� ���������� �� ����
	 * @param f ���������� ��������� ����������� �� ������
	 */
	void saving(FILE *f);
	
	/*!
	 * @fn void loading(FILE *f)
	 * @brief �������� ����������� � �����
	 * @param f ���������� ��������� ����������� �� ������
	 */
	void loading(FILE *f);

	friend class Fs;
};

/*!
 * @class Fs
 * @brief ����� �������� �������
 */
class Fs
{
	vector <File> Files;			//!< ��� ����� ��
	vector <Directory> Dirs;		//!< ��� ���������� ��
	vector <int> n_Blocks;			//!< ���������� ������ � ������ ������
	vector <Block> Blocks;			//!< ��� ����� ��

	string path_fs;					//!< ������� �������

public:
	/*!
	 * @fn Fs()
	 * @brief ����������� ��
	 */
	Fs();

	/*!
	 * @fn void add_dir(string name_dir, int where_dir)
	 * @brief ������� ��������� � ��������
	 * @param name_dir ��� ����������
	 * @param where_dir ����� ���������� ����
	 */
	void add_dir(string name_dir, int where_dir);
	 
	/*!
	 * @fn void add_file(string name_file, int where_dir)
	 * @brief ������� ���� � ��������
	 * @param name_file ��� �����
	 * @param where_dir ����� ���������� ����
	 */
	void add_file(string name_file, int where_dir);

	/*!
	 * @fn int get_n_dir(string name_dir)
	 * @brief ��������� ����� ���������� �� �����
	 * @param name_dir ��� ����������
	 * @return ����� ��� -1
	 */
	int get_n_dir(string name_dir);
	
	/*!
	 * @fn int get_n_file(string name_file)
	 * @brief ��������� ����� ����� �� �����
	 * @param name_file ��� �����
	 * @return ����� ��� -1
	 */
	int get_n_file(string name_file);
			
	/*!
	 * @fn vector<string> print_tree(string prefix)
	 * @brief ��������� ����������� ���������� ������� ��������
	 * @param prefix ����� �������� ������� ������� �������� ����� �������
	 * @return vector ����� ����������
	 */
	vector<string> print_tree(string prefix);

	/*! 
	 * @fn vector<string> print_dir(int n_dir)
	 * @brief ��������� ���������� ������� ��������
	 * @param n_dir ����� ����������
	 * @return vector ����� ����������
	 */
	vector<string> print_dir(int n_dir);
	
	/*!
	 * @fn vector<string> print_tree(int n_dir)
	 * @brief ��������� ����������� ���������� ��������� ��������
	 * @param n_dir ����� ����������
	 * @return vector ����� ����������
	 */
	vector<string> print_tree();
	
	/*!
	 * @fn vector<string> print_tree(int n_dir)
	 * @brief ��������� ����������� ���������� ������� ��������
	 * @param n_dir ����� ����������
	 * @return vector ����� ����������
	 */
	vector<string> print_tree(int n_dir);

	/*!
	 * @fn vector<string> ls()
	 * @brief ��������� ���������� �������� ��������
	 * @return vector ����� ����������
	 */
	vector<string> ls();
	
	/*!
	 * @fn void cd(string the_path)
	 * @brief ����������� �� ������ ���������
	 * @param the_path ����� ����
	 */
	void cd(string the_path);
	
	/*!
	 * @fn void mkdir(string the_name_dir)
	 * @brief ������� ������������� � ������� ��������
	 * @param the_name_dir ��� ����������
	 */
	void mkdir(string the_name_dir);
	
	/*!
	 * @fn void touch(string the_name_file);
	 * @brief ������� ���� � ������� ��������
	 * @param the_name_file ��� �����
	 */
	void touch(string the_name_file);
	
	/*!
	 * @fn void rm(string the_name_file)
	 * @brief ������� ���� �� �������� ��������
	 * @param the_name_file ��� �����
	 */
	void rm(string the_name_file);
	
	/*!
	 * @fn void rmdir(string the_name_dir)
	 * @brief ������� ���������� �� �������� ��������
	 * @param the_name_dir ��� �����������
	 */
	void rmdir(string the_name_dir);

	/*!
	 * @fn int get_file_size(string the_file_name)
	 * @brief �������� ������ �����
	 * @param the_file_name ��� ����
	 * @return ������
	 */
	int get_file_size(string the_file_name);
	
	/*!
	 * @fn int set_file_size(string the_file_name, int the_size)
	 * @brief �������� ������ �����
	 * @param the_file_name ��� ����
	 * @param the_size ������
	 * @return ������� ������� ��
	 */
	int set_file_size(string the_file_name, int the_size);
	
	/*!
	 * @fn int set_file_size(string the_file_name, int the_size)
	 * @brief �������� ������ ����� �� ...
	 * @param the_file_name ��� ����
	 * @param the_size ������ �������
	 * @return ������� ������� ��
	 */
	int add_size(int n_block_where, int the_size);
	
	/*!
	 * @fn int get_n_last_blosk(int the_n);
	 * @brief ��������� ����� ���������� ����� ������� ������
	 * @param the_n - ����� ������ �� ������ ������
	 * @return ����� ���������� �����
	 */
	int get_n_last_blosk(int the_n);

	/*!
	 * @fn void cpin(string out_file_name, string in_file_name)
	 * @brief ����������� ������� ���� �� ���������� ���� ��
	 * @param out_file_name ������� ����
	 * @param in_file_name ���������� ����
	 */
	void cpin(string out_file_name, string in_file_name);
	
	/*!
	 * @fn void cpout(string in_file_name, string out_file_name);
	 * @brief ����������� ���������� ���� �� � ������� ����
	 * @param in_file_name ���������� ����
	 * @param out_file_name ������� ����
	 */
	void cpout(string in_file_name, string out_file_name);
	
	/*!
	 * @fn void cpadd(string in_file_name, string data)
	 * @brief �������� ������ � ����
	 * @param in_file_name ���������� ����
	 * @param data ������
	 */
	void cpadd(string in_file_name, string data);
	
	/*!
	 * @fn void cpprint(string in_file_name)
	 * @brief ������� ���� �� �����
	 * @param in_file_name ���������� ����
	 */
	void cpprint(string in_file_name);
	
	/*!
	 * @fn cpfind(string in_file_name, string find);
	 * @brief ������� �� ����� ������ ���������� ���������
	 * @param in_file_name ���������� ����
	 * @param find ���������
	 */
	void cpfind(string in_file_name, string find);
	
	/*!
	 * @fn void cpinfo(string in_file_name)
	 * @brief ������� ���������� �� ����������� ����
	 * @param in_file_name ���������� ����
	 */
	void cpinfo(string in_file_name);

	
	/*!
	 * @fn void saving(FILE *f)
	 * @brief ��������� ���������� �� ����
	 * @param f ���������� ��������� ����������� �� ������
	 */
	void saving(FILE *f);
	
	/*!
	 * @fn void loading(FILE *f)
	 * @brief �������� ����������� � �����
	 * @param f ���������� ��������� ����������� �� ������
	 */
	void loading(FILE *f);
	
	/*!
	 * @fn void set_path_fs(string the_path_fs)
	 * @brief �������� ������� ����
	 * @param the_path_fs ����� ��������
	 */
	void set_path_fs(string the_path_fs);
	
	/*!
	 * @fn string get_path_fs()
	 * @brief ��������� ������� ����
	 * @return �������� ���������� �������� ����
	 */
	string get_path_fs();

	friend class Directory;
};

#endif
