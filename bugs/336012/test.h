
/// class ::A
class A {
public:
 A();
};

/// class ::B1
class B1 {
public:
  B1();
  function(const D1::C &test);
};

/// class ::B2
class B2 {
public:
  B2();
  function(const D2::C &test);
};

/// class ::B3
class B3 {
public:
  B3();
  function(const D3::C &test);
};

// D1 is undefined

/// class ::D2
class D2 {
public:
/// class D2::C
class C { public: C(); };
};


/// namespace D3
namespace D3 {
/// class D3::C
class C { public: C(); };
}

/// namespace D4
namespace D4 {
/// class D4::C
class C { public: C(); };
}
