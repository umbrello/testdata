/**
 * File:   JackData.h
 * Author: flavio
 *
 * Created on 24 de Julho de 2010, 06:29
 */

#ifndef _JACKAUDIODATA_H
#define	_JACKAUDIODATA_H

#include <QString>
#include <jack/types.h>
#include <sys/types.h>
#include <malloc.h>

namespace sound {

    /**
     * Esta classe representa os dados a serem colocados no Buffer circular.
     * Ela guarda a posição do transporte no Jack, o número da porta do dado, a quantidade de frames
     * e os dados propriamente ditos.
     */
    class JackAudioData {
    public:
        /**
         * Construtor.
         * Recebe como parâmetro o tamanho dos dados no jack.
         */
        JackAudioData(jack_nframes_t nframes);
        /**
         * Construtor.
         * Recebe como parâmetro uma instância estática da classe.
         */
        JackAudioData(const JackAudioData& orig);
        /**
         * Construtor.
         * Recebe como parâmetro uma instância estática da classe.
         */
        JackAudioData(JackAudioData * orig);
        /**
         * Construtor.
         * Recebe como parâmetro todos os dados encapsulados pela classe.
         */
        JackAudioData(int stream, jack_nframes_t tempo, jack_default_audio_sample_t * dados, jack_nframes_t nframes);
        /**
         * Destrutor padrão da classe.
         */
        ~JackAudioData();
        /**
         * Retorna o tempo do transporte do jack deste dados de áudio.
         */
        jack_nframes_t getTempo();
        /**
         * Retorna o tamanho do buffer interno do jack.
         */
        jack_nframes_t getNFrames();
        /**
         * Retorna os dados de áudio.
         */
        jack_default_audio_sample_t * getDados();
        /**
         * Retorna o número da stream deste trecho de áudio.
         * Este número também corresponde ao número da porta de áudio no jack.
         */
        int getStream();
        /**
         * Define o número da stream deste trecho de áudio.
         * Este número também corresponde ao número da porta de áudio no jack.
         */
        void setStream(int stream);
        /**
         * Define os dados deste trecho de áudio.
         */
        void setDados(jack_default_audio_sample_t * dados);

    private:
        jack_nframes_t tempo;
        jack_nframes_t nframes;
        jack_default_audio_sample_t * dados;
        int stream;
    };
};
#endif	/* _JACKAUDIODATA_H */