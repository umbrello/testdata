
typedef int IntType;
typedef int* IntTypePointer;
typedef int& IntTypeAddress;

class TestInt {
public:
    void test1(IntType param);
    void test1(IntTypePointer param);
    void test1(IntTypeAddress param);
};

class Test;
typedef Test TestClassType;
typedef Test* TestClassTypePointer;
typedef Test& TestClassTypeAddress;

typedef TestClassType TestClassTypeType;
typedef TestClassTypePointer TestClassTypePointerType;
typedef TestClassTypeAddress TestClassTypeAddressType;

class TestClass {
public:
    void test4(Test param);
    void test4(Test& param);
    void test4(Test* param);
    void test5(TestClassType param);
    void test5(TestClassTypePointer param);
    void test5(TestClassTypeAddress param);
    void test6(TestClassTypeType param);
    void test6(TestClassTypePointerType param);
    void test6(TestClassTypeAddressType param);
};

#include "/usr/include/QtCore/qlist.h"
typedef QList<Test> TestList;
typedef QList<Test>* TestListPointer;
typedef QList<Test>& TestListAddress;

class TestQList {
public:
    void test2(QList<Test> param);
    void test2(QList<Test>* param);
    void test2(QList<Test>& param);
    void test3(TestList param);
    void test3(TestListPointer param);
    void test3(TestListAddress param);
};


class TestClass : public TestList {
public:
    TestClass() {}
};

