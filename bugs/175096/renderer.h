#ifndef RENDERER_H
#define RENDERER_H



#include <string>
#include <list>
#include <map>
#include <memory>


// has to be loaded before glfw
#include <gl_3_3.h>
#include <gll.hpp>
#include <glm.hpp>
#include <gtx/transform.hpp>
#include <gtc/matrix_transform.hpp>
#include <GL/glfw.h>
#include <sigc++/sigc++.h>

/// project includes
#include "console.h"
#include "gl_context.h"
#include "renderable.h"
#include "shader_loader.h"
#include "view_transforms.h"


using namespace std;



class Renderer : public sigc::trackable
{
  public:

    Renderer();
    ~Renderer();

    void init(sigc::slot<void,int,string,string>);

    void create_gl_context();
    void destroy_gl_context();

    void render_frame();
    int get_frame();
    void quit();

    void set_fullscreen(bool);


    glm::mat4 get_view_matrix();
    void set_view_matrix(glm::mat4);
    glm::mat4 get_projection_matrix();
    void set_projection_matrix(glm::mat4);
    glm::mat4 get_mvp();
    void set_mvp(glm::mat4);

    void add_shader(string, GLuint);
    GLuint get_shader(string);
    void list_shaders();

    View_transforms* get_view_transforms_ptr();
    Gl_context* get_gl_context();

    int get_screen_width();
    void set_screen_width(int);
    int get_screen_height();
    void set_screen_height(int);

    void render_scene();

    void toggle_debug_console();

    void save_screenshot();


    shared_ptr<Renderable> new_renderable(sigc::slot<void,int,string,string>,string,string,glm::vec3,View_transforms*); //renderable class name, shader_id name, translation vector, view transforms pointer

    void delete_renderable(int);
    shared_ptr<Renderable> get_renderable_ptr(int); // get by id
    void set_renderable_translation(int, glm::vec3); // set by id

    //shader programs
    map<string, GLuint> shaders; //
    map<string, GLuint>::iterator shaders_iter; //

    GLuint MatrixID;
    GLuint ViewMatrixID;
    GLuint ModelMatrixID;

    GLuint LightID;
    glm::vec3 lightPos;

    string t;

    void on_keypress(int);

    Gl_context gl_context;
    View_transforms view_transforms;
    Console console;

    sigc::signal<void,int,string,string> signal_log_msg;



  protected:

  private:

    string log_msg;
    ostringstream log_msg_oss;

    Shader_loader shader_loader;

    int renderables_map_id=0;
    map<int,shared_ptr<Renderable>> renderables;
    map<int,shared_ptr<Renderable>>::iterator renderables_iter;

    int frame=0;
    float fps=0;


    bool display_debug_console=false;

};

#endif
