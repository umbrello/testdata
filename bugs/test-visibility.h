// file test-visibility.h

/// LogicalView::Test[class]
class Test {
public:
	/// LogicalView::Test::publicMethod[method]{public}
	void publicMethod();
	/// LogicalView::Test::publicMember[member]{public}
	bool publicMember;
protected:
	/**
	 * @parent Test 
	 * @name protectedMethod 
	 * @type method 
	 * @scope protected
	 */
	void protectedMethod();
	/// LogicalView::Test::protectedMember[member]{protected}

	/// @parent Test @name protectedMember @type member @scope protected
	bool protectedMember;
private:
	/// LogicalView::Test::privateMethod[method]{private}
	void privateMethod();
	/// LogicalView::Test::privateMember[member]{private}
	bool privateMember;
};


class Test2 : private Test {
protected:
	Test2();
};
