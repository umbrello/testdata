class foo
{
public:
    foo();
    ~foo();
};
class bar
{
public:
    typedef std::list<foo*> foolist;
    bar();
    ~bar;
private:
    foolist the_list;
};
class rebar
{
public:
    rebar();
    ~rebar;
private:
    std::list<foo*> the_list;
};
