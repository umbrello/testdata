 /// class Global::A 
class A { public: A(); }; 


namespace N1 { 
	/// class N1::A 
	class A { public: A(); }; 
} 

namespace N2 { 
	/// class N2::A 
	class A { public: A(); }; 
} 


namespace N3 { 
	/// class N3::A 
	class A { public: A(); }; 
	/// class N3::B 
	class B : public A { public: B(); }; 
	/// class N3::C 
	class C : public ::A { public: C(); }; 
	/// class N3::D 
	class D : public N1::A { public: D(); }; 
	/// class N3::E 
	class E : public N2::A { public: E(); }; 

}