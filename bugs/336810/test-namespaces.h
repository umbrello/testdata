/// class Global::A 
class A { public: A(); }; 

namespace N1 { 
    /// class N1::A 
    class A { public: A(); }; 
    
} 

namespace N2 { 
    /// class N2::A 
    class A { public: A(); }; 
    /// class N2::B 
    class B : public A { public: B(); }; 
    /// class N2::C 
    class C : public ::A { public; C(); }; 
    /// class N2::D 
    class C : public N1::A { public; C(); }; 
}
