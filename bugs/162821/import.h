namespace Rtdb {

class CDriver;

class CAddress : public ::CAddress {
friend class Rtdb::CDriver;
public:
    CAddress();
    CAddress(int a_address);

    virtual bool Equals(::CAddress& a_address);
    virtual int Compare(::CAddress& a_address);
    virtual bool Serialize(CXMLArchive*& a_par);
    virtual const char* ToString();
    virtual void Read(Head& a_head);
    virtual void Write(Head& a_head);

private:
    int address_;
};

}
