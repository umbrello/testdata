
public class DrawPanel extends JPanel implements Runnable {
    private final class DrawPanelKeyListener implements KeyListener {
        private final DrawPanel dp; /* synthetic field */
        
        public DrawPanelKeyListener(DrawPanel dp) {
            this.dp = dp;
        }
    }
    
    private final class DrawPanelMouseListener implements MouseListener{

        private final DrawPanel dp; /* synthetic field */
        
        public DrawPanelMouseListener(DrawPanel dp) {
            this.dp = dp;
        }
    }
    
    private final class DrawPanelMouseMotionListener implements MouseMotionListener{
        
        private final DrawPanel dp; /* synthetic field */

        public DrawPanelMouseMotionListener(DrawPanel dp) {
            this.dp = dp;
        }
    }

    private final class DrawPanelComponentListener implements ComponentListener{

        @Override
        public void componentHidden(ComponentEvent e) {
            // TODO Auto-generated method stub
            
        }
    }

    
    private final class DialogPlansActionListener implements ActionListener {

        private final DrawPanel dp; /* synthetic field */

        DialogPlansActionListener(DrawPanel a450_1) {
            dp = a450_1;
        }
    }

    private final class DialogEbenenActionListener implements ActionListener {

        private final DrawPanel dp; /* synthetic field */

        DialogEbenenActionListener(DrawPanel a450_1) {
            dp = a450_1;
        }
    }

    private final class DialogFoilsActionListener implements ActionListener {

        private final DrawPanel dp; /* synthetic field */

        DialogFoilsActionListener(DrawPanel a450_1) {
            dp = a450_1;
        }
    }

    private final class DialogPrintActionListener implements ActionListener {
        private final class DialogPrintInfoActionListener implements ActionListener {

            private final DrawPanel dp; /* synthetic field */

            DialogPrintInfoActionListener(DrawPanel a450_1) {
                dp = a450_1;
            }
        }

        private final DrawPanel dp; /* synthetic field */

        DialogPrintActionListener(DrawPanel a450_1) {
            dp = a450_1;
        }
    }
        
    class ConfigFiles {
        public String plansIniFile = null;
        public String toolBarIniFile = null;

        public ConfigFiles(DrawPanel a450_1, String s, String s1) {
            plansIniFile = s;
            toolBarIniFile = s1;
        }
    }
    
    public DrawPanel(Gausz gausz) throws IOException {
        super();
    }
}
