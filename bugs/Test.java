import java.io.*;

public class Test
{
  public static void main( String[] args )
  {
    System.out.println( "Text eingeben und mit Return abschliessen:" );
    try {
      BufferedReader in = new BufferedReader(
                          new InputStreamReader( System.in ) );
      String s = in.readLine();
      System.out.println( "Der eingelesene Text lautet: " + s );
    } catch( IOException ex ) {
      System.out.println( ex.getMessage() );
    }
  }
}